package ru.tinkoff.bankaccount

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}
import ru.tinkoff.bankaccount.AccountType.{Saving}

class ExtendServiceTest extends FlatSpec with Matchers {
  "Extend service" should "shift endDate for active account" in {
    val endDate = LocalDate.now().plusMonths(9)
    val savingAcc = DepositAccount(0, LocalDate.of(2017, 12, 12), endDate, Saving, AccountTariff("someTariff"))

    val newEndDate = ExtendService.extendAccount(savingAcc).endDate

    assert(newEndDate.isAfter(endDate))
  }

  "Extend service" should "shift endDate for closed account" in {
    val now = LocalDate.now()
    val endDate = now.minusYears(1)
    val savingAcc = DepositAccount(0, LocalDate.of(2017, 12, 12), endDate, Saving, AccountTariff("someTariff"))

    val newEndDate = ExtendService.extendAccount(savingAcc).endDate

    assert(newEndDate.isAfter(now))
  }
}
