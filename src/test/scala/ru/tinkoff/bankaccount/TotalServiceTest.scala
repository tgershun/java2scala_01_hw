package ru.tinkoff.bankaccount

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}

class TotalServiceTest extends FlatSpec with Matchers {

  val debitAccount = DebitAccount(30, LocalDate.of(2019, 12, 12), LocalDate.of(2023, 12, 12))
  val creditAccount = CreditAccount(-20, LocalDate.of(2019, 12, 12), LocalDate.of(2023, 12, 12))
  val nonActiveAccount = DepositAccount(0.0, LocalDate.of(2019, 12, 12), LocalDate.of(2023, 12, 12))

  "Total service" should "get total balance of all the accounts" in {
    val res = TotalBalanceService.getTotal(Seq(debitAccount, creditAccount, nonActiveAccount))
    res shouldBe 10
  }
}
