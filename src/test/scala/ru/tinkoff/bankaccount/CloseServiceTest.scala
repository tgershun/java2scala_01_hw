package ru.tinkoff.bankaccount

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}
import ru.tinkoff.bankaccount.AccountType.{Deposit, Saving}

class CloseServiceTest extends FlatSpec with Matchers {

  "Close service" should "close active Saving or Deposit account" in {
    val savingAccToClose = SavingAccount(0, LocalDate.of(2019, 12, 12), LocalDate.of(2023, 12, 12), Saving, AccountTariff("someTariff"))
    val depositAccToClose = DepositAccount(0, LocalDate.of(2019, 12, 12), LocalDate.of(2023, 12, 12), Deposit, AccountTariff("someTariff"))
    val expectedClosedSavingAcc = SavingAccount(0, LocalDate.of(2019, 12, 12), LocalDate.now(), Saving, AccountTariff("someTariff"))
    val expectedClosedDepositAcc = DepositAccount(0, LocalDate.of(2019, 12, 12), LocalDate.now(), Deposit, AccountTariff("someTariff"))

    expectedClosedSavingAcc shouldBe  CloseService.closeAccount(savingAccToClose)
    expectedClosedDepositAcc shouldBe CloseService.closeAccount(depositAccToClose)
  }

  "Close service" should "leave account as is when closed already" in {
    val savingAccToClose = SavingAccount(0, LocalDate.of(2019, 12, 12), LocalDate.of(2017, 12, 12), Saving, AccountTariff("someTariff"))
    val depositAccToClose = DepositAccount(0, LocalDate.of(2019, 12, 12), LocalDate.of(2018, 12, 12), Deposit, AccountTariff("someTariff"))

    savingAccToClose shouldBe CloseService.closeAccount(savingAccToClose)
    depositAccToClose shouldBe CloseService.closeAccount(depositAccToClose)
  }

  "Close service" should "throw an exception when closing account with non-zero value" in {
    val savingAccToClose = SavingAccount(30, LocalDate.of(2019, 12, 12), LocalDate.of(2024, 12, 12), Saving, AccountTariff("someTariff"))

    val creditCaught =
      intercept[Exception] {
        CloseService.closeAccount(savingAccToClose)
      }
    assert(creditCaught.getMessage == "Cannot close account with non-zero balance")
  }
}
