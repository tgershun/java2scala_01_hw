package ru.tinkoff.bankaccount

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}
import ru.tinkoff.bankaccount.AccountType.{Credit, Deposit, Saving}

class OpenServiceTest extends FlatSpec with Matchers {

  "Open service" should "open service with given type" in {
    val accountType = Deposit
    val initialBalance = 40

    val account = OpenService.openAccount(accountType, initialBalance)
    assert(account.endDate.isAfter(LocalDate.now))
    assert(account.balance == initialBalance)
  }

  "Open service" should "throw an exception while opening credit account with positive balance" in {
    val accountType = Credit
    val initialBalance = 40

    val creditCaught =
      intercept[Exception] {
        OpenService.openAccount(accountType, initialBalance)
      }
    assert(creditCaught.getMessage == "Initial balance for Credit type should be 0")
  }

  "Open service" should "open credit account" in {
    val accountType = Credit
    val initialBalance = 0


    val creditAcc = OpenService.openAccount(accountType, initialBalance)

    assert(creditAcc.endDate.isAfter(LocalDate.now))
    creditAcc.balance shouldBe initialBalance
  }

  "Open service" should "throw an exception while opening non-credit account with negative balance" in {
    val accountType = Saving
    val initialBalance = -40

    val creditCaught =
      intercept[Exception] {
        OpenService.openAccount(accountType, initialBalance)
      }
    creditCaught.getMessage shouldBe "Initial balance for non-Credit type should be above or equals to 0"
  }
}
