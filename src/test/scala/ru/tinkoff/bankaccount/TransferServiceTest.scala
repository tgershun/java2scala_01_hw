package ru.tinkoff.bankaccount

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}

class TransferServiceTest extends FlatSpec with Matchers {
  val from = DebitAccount(30, LocalDate.of(2019, 12, 12), LocalDate.of(2023, 12, 12))
  val to = SavingAccount(30, LocalDate.of(2019, 12, 12), LocalDate.of(2023, 12, 12))

  "Transfer service" should "transfer properly" in {
    val res = TransferService.transfer(20, from, to)

    val expectedFrom = from.copy(balance = 10)
    val expectedTo = to.copy(balance = 50)

    (expectedFrom, expectedTo) shouldBe res
  }

  "Transfer service" should "throw an exception when transfer amount exceed balance" in {
    val notEnoughMoneyAcc = DebitAccount(10, LocalDate.now(), LocalDate.now().plusYears(3))
    val caught =
      intercept[Exception] {
        TransferService.transfer(20, notEnoughMoneyAcc, to)
      }
    caught.getMessage shouldBe "Not enough balance to charge 20"
  }

  "Transfer service" should "throw an exception when transfer wrong amount to Credit account" in {
    val toAcc = CreditAccount(-10, LocalDate.now(), LocalDate.now().plusYears(3))
    val caught =
      intercept[Exception] {
        TransferService.transfer(20, from, toAcc)
      }
    caught.getMessage shouldBe "Recharged amount for credit account cannot exceed it's current balance"
  }

  "Transfer service" should "throw an exception when one of the accounts is closed" in {
    val toAcc = SavingAccount(50, LocalDate.now(), LocalDate.now().minusYears(3))
    val caught =
      intercept[Exception] {
        TransferService.transfer(20, from, toAcc)
      }
    caught.getMessage shouldBe "Source and destination accounts should be active"
  }

  "Transfer service" should "transfer to Credit account without exceptions" in {
    val toAcc = CreditAccount(-10, LocalDate.now(), LocalDate.now().plusYears(3))

    val expectedFrom = from.copy(balance = 20)
    val expectedTo = toAcc.copy(balance = 0)

    (expectedFrom, expectedTo) shouldBe TransferService.transfer(10, from, toAcc)
  }

}
