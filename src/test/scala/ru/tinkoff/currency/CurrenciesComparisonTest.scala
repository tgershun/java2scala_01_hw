package ru.tinkoff.currency

import org.scalatest.{FlatSpec, Matchers}
import ru.tinkoff.currency.CompareResult._
import ru.tinkoff.currency.Currency._

class CurrenciesComparisonTest extends FlatSpec with Matchers {

  "Compare type class" should "compare by rates properly" in {
    import Compare.ComparatorByRate

    Usd.compare(Rub) == Greater shouldBe true
    Usd.compare(Eur) == Less shouldBe true
    Eur.compare(Usd) == Greater shouldBe true
    Eur.compare(Rub) == Greater shouldBe true
    Rub.compare(Rub) == Equals shouldBe true
  }

  "Compare type class" should "compare by names properly" in {
    import Compare.ComparatorByName

    Usd.compare(Rub) == Greater shouldBe true
    Usd.compare(Eur) == Greater shouldBe true
    Eur.compare(Usd) == Less shouldBe true
    Eur.compare(Rub) == Less shouldBe true
    Rub.compare(Rub) == Equals shouldBe true
  }
}
