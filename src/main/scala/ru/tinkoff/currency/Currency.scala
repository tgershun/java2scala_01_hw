package ru.tinkoff.currency

object Currency {

  case object Usd extends Currency

  case object Eur extends Currency

  case object Rub extends Currency

}

sealed trait Currency