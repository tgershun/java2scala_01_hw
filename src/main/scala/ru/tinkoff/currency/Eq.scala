package ru.tinkoff.currency

import ru.tinkoff.currency.Currency.{Rub, Usd}

trait Eq[T] {
  def compare(left: T, right: T): Boolean
}

object Eq {

  implicit class EqSyntax[T](val left: T) { // extension method for nice syntax
    def ====(right: T)(implicit eq: Eq[T]): Boolean = eq.compare(left, right)
  }

}

trait EqInstances {
  implicit val currencyEq: Eq[Currency] = new Eq[Currency] {
    override def compare(left: Currency, right: Currency): Boolean = left == right
  }
}

object EqInstances extends EqInstances


object Main extends App {

  import Eq._
  import EqInstances._

  val rub: Currency = Rub
  val usd: Currency = Usd

  assert(!(rub ==== usd))
}