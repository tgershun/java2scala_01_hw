package ru.tinkoff.currency

import ru.tinkoff.currency.Currency._
//TODO: добавать инстансы для всех валют
// Нельзя менять евро на доллар и доллар на евро.
// Нельзя менять валюту саму на себя
// Курсы покупки и продажи отличаются

trait Rate[S, T] {
  val rate: BigDecimal
}

object Rate {

  // todo: Use something more specific than Currency. val UsdRub: Rate(Usd.type, Rub,type) = () => BigDecimal(65.56)
  object UsdRub extends Rate[Currency, Currency] {
    val rate: BigDecimal = BigDecimal(65.56)
  }

  object EurRub extends Rate[Currency, Currency] {
    val rate: BigDecimal = BigDecimal(75.08)
  }

  object RubUsd extends Rate[Currency, Currency] {
    val rate: BigDecimal = 1 / BigDecimal(68.56)
  }

  object RubEur extends Rate[Currency, Currency] {
    val rate: BigDecimal = 1 / BigDecimal(77.54)
  }

  // to have an ability to compare any Currency with Rub by rates...
  object RubRub extends Rate[Currency, Currency] {
    val rate: BigDecimal = 1
  }

  //todo try to rule out getRate. Feels like could be done shorter. without such method
  def getRate[C1 <: Currency, C2 <: Currency](from: C1, to: C2): Rate[Currency, Currency] = {
    (from, to) match {
      case (Usd, Rub) => UsdRub
      case (Eur, Rub) => EurRub
      case (Rub, Usd) => RubUsd
      case (Rub, Eur) => RubEur
      case (Rub, Rub) => RubRub
      case _ => throw new IllegalArgumentException(s"Rate is not specified from $from to $to")
    }
  }
}