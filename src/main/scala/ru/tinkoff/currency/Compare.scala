package ru.tinkoff.currency

import ru.tinkoff.currency.CompareResult.{Equals, Greater, Less}
import ru.tinkoff.currency.Currency.{Eur, Rub, Usd}

// TODO: Необходимо реализовать сравнения для валют по их имени
// 1. Сравнение по названию валют
// 2. Сравнение валют по курсу валют в рублях. Курс нужно брать из Rate[A, B]
// PS: Подумайте хорошо над тем, что должно быть под ???
trait Compare[C1 <: Currency, C2 <: Currency] {
  def compare(left: C1, right: C2): CompareResult
}

object Compare {

  implicit class Converter(result: Int) {
    def convertToCompareResult: CompareResult = result match {
      case 0 => Equals
      case r if r > 0 => Greater
      case r if r < 0 => Less
    }
  }

  implicit class ComparatorByRate[C1 <: Currency](val c1: C1) {
    def compare[C2 <: Currency](c2: C2): CompareResult = new Compare[C1, C2] {
      override def compare(left: C1, right: C2): CompareResult =
        Rate.getRate(left, Rub).rate.compareTo(Rate.getRate(right, Rub).rate).convertToCompareResult
    }.compare(c1, c2)
  }

  implicit class ComparatorByName[C1 <: Currency](val c1: C1) {
    def compare[C2 <: Currency](c2: C2): CompareResult = new Compare[C1, C2] {
      override def compare(left: C1, right: C2): CompareResult =
        c1.toString.toLowerCase.compareTo(c2.toString.toLowerCase).convertToCompareResult
    }.compare(c1, c2)
  }

}

sealed trait CompareResult

object CompareResult {

  case object Less extends CompareResult

  case object Greater extends CompareResult

  case object Equals extends CompareResult

}

//TODO
object CompareApp extends App {
  // Not sure what was the expectation
  // Implementing this way and passing Currencies, I will always need to import either ComparatorByRate or ComparatorByName
  // If the expectation is to handle particular impl automatically
  // I will need to pass different parameters to distinguish what impl to use
  // seems then bounds for c1 and c2 have to be changed
  import Compare.ComparatorByRate

  require(Usd.compare(Rub) == Greater)
  require(Usd.compare(Eur) == Less)
}
