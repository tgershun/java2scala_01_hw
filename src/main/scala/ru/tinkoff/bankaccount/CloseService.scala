package ru.tinkoff.bankaccount

import java.time.LocalDate

object CloseService {
  def closeAccount(accountToClose: ClosableAccount): ClosableAccount = {

    if (accountToClose.isClosed) {
      accountToClose
    } else {
      checkBalance(accountToClose)
      accountToClose match {
        case DepositAccount(balance, startDate, _, accountType, accountTariff) => DepositAccount(balance, startDate, LocalDate.now(), accountType, accountTariff)
        case SavingAccount(balance, startDate, _, accountType, accountTariff) => SavingAccount(balance, startDate, LocalDate.now(), accountType, accountTariff)
      }
    }
  }

  private def checkBalance(accountToClose: ClosableAccount): Unit = {
    if (accountToClose.balance != 0) {
      throw new Exception(s"Cannot close account with non-zero balance")
    }
  }
}
