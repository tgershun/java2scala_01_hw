package ru.tinkoff.bankaccount

import java.time.LocalDate

import enumeratum._

/*
Требуется смоделировать следующий домен.

Банковские счета:
У каждого человека может быть несколько счетов.
У каждого счета есть баланс, дата открытия, дата закрытия, тип, тариф
Счета бывают 4 типов: обычные, накопительные, кредитные, вклады.
Кредитный счет всегда имеет отрицательный или нулевой баланс.

Реализовать сервисы:
1. Суммарный баланс по всем счетам. Принимает на вход список счетов и возвращает число.
2. Пополнение счетов. Принимает на вход счет источник и счет получатель.
    * Источником может быть только обычный счет или накопительный
    * Получателем - любой счет. Кредитный счет нельзя пополнить более, чем на сумму долга.
// т.к. пункт 4, в котором речь все-таки не только о вкладах, импл сервиса на все типы
3. Сервис продления вклада: принимает счет и возвращает счет.
// Закрываем вклады, но счет принимаем еще и накопительный. Сделано для накопительный вклад
4. Сервис закрытия вкладов. Принимает либо накопительный счет либо вклад. Возвращает счет.
5. Сервис открытия счета: принимает тип счета и возвращает счет.


Написать на scalatest:
Тесты на все сервисы.

 */
// didn't understand the purpose of the tariff. No info/goal in the task. so just a String
// constraint for credit is not checked but opening account it starts from 0 and constraints checked on transfer action
sealed trait BankAccount {
  val balance: BigDecimal
  val startDate: LocalDate
  val endDate: LocalDate
  val accountType: AccountType
  val tariff: AccountTariff

  lazy val isClosed = endDate.isBefore(LocalDate.now())
}

sealed trait ClosableAccount extends BankAccount
sealed trait ChargeableAccount extends BankAccount

case class SavingAccount(balance: BigDecimal, startDate: LocalDate, endDate: LocalDate, accountType: AccountType = AccountType.Saving, tariff: AccountTariff = AccountTariff("SomeSavingTariff")) extends ClosableAccount with ChargeableAccount

case class CreditAccount(balance: BigDecimal, startDate: LocalDate, endDate: LocalDate, accountType: AccountType = AccountType.Credit, tariff: AccountTariff = AccountTariff("SomeCreditTariff")) extends BankAccount

case class DebitAccount(balance: BigDecimal, startDate: LocalDate, endDate: LocalDate, accountType: AccountType = AccountType.Debit, tariff: AccountTariff = AccountTariff("SomeDebitTariff")) extends ChargeableAccount

case class DepositAccount(balance: BigDecimal, startDate: LocalDate, endDate: LocalDate, accountType: AccountType = AccountType.Deposit, tariff: AccountTariff = AccountTariff("SomeDepositTariff")) extends ClosableAccount

//some tariff with name
case class AccountTariff(tariffName: String)

sealed trait AccountType extends EnumEntry

object AccountType extends Enum[AccountType] {
  val values = findValues

  case object Saving extends AccountType

  case object Credit extends AccountType

  case object Debit extends AccountType

  case object Deposit extends AccountType

}