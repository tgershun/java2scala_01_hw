package ru.tinkoff.bankaccount

import java.time.LocalDate

object ExtendService {
  val DefaultExtendAmountInMonths = 6

  // сервис продления вклада, должен работать только на вклад
  def extendAccount(account: DepositAccount): DepositAccount = {
    val now = LocalDate.now()
    // perhaps it makes sense to extend closed account or almost closed
    // get max to add months to either closed or active account
    val date: LocalDate = if (now.compareTo(account.endDate) > 0) now else account.endDate
    account.copy(endDate = date.plusMonths(DefaultExtendAmountInMonths))
  }
}