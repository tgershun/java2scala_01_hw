package ru.tinkoff.bankaccount

object TotalBalanceService {
  def getTotal(accounts: Seq[BankAccount]): BigDecimal = {
    // closed accounts should have zero balance so no need in filter
    accounts.map(_.balance).sum
  }
}
