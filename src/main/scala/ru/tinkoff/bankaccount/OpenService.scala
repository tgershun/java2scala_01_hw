package ru.tinkoff.bankaccount

import java.time.LocalDate

import ru.tinkoff.bankaccount.AccountType._

object OpenService {
  val DefaultActiveYears = 2

  def openAccount(accountType: AccountType, initialBalance: BigDecimal): BankAccount = {
    accountType match {
      case Credit =>
        if (initialBalance != 0) throw new Exception("Initial balance for Credit type should be 0") else {
          CreditAccount(0, LocalDate.now, LocalDate.now.plusYears(DefaultActiveYears))
        }
      case Debit if initialBalance >= 0 =>
        DebitAccount(initialBalance, LocalDate.now, LocalDate.now.plusYears(DefaultActiveYears))
      case Saving if initialBalance >= 0 =>
        DebitAccount(initialBalance, LocalDate.now, LocalDate.now.plusYears(DefaultActiveYears))
      case Deposit if initialBalance >= 0 =>
        DebitAccount(initialBalance, LocalDate.now, LocalDate.now.plusYears(DefaultActiveYears))
      case _ =>
        throw new Exception("Initial balance for non-Credit type should be above or equals to 0")
    }
  }
}
