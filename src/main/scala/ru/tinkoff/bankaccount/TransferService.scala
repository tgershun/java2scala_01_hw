package ru.tinkoff.bankaccount

import ru.tinkoff.bankaccount.AccountType.Credit

object TransferService {
  def transfer(amount: BigDecimal, from: ChargeableAccount, to: BankAccount): (ChargeableAccount, BankAccount) = {
    val result = for {
      _ <- if (from.isClosed || to.isClosed) Left("Source and destination accounts should be active") else Right()
      _ <- checkSourceBalance(from, amount)
      _ <- checkDestinationAccountBalance(to, amount)
    } yield (updateFrom(from, amount), updateTo(to, amount))

    result match {
      case Left(msg) => throw new Exception(msg)
      case Right(accounts) => accounts
    }
  }

  private def updateFrom(from: ChargeableAccount, amount: BigDecimal): ChargeableAccount = {
    from match {
      case SavingAccount(balance, startDate, endDate, accountType, accountTariff) => SavingAccount(balance - amount, startDate, endDate, accountType, accountTariff)
      case DebitAccount(balance, startDate, endDate, accountType, accountTariff) => DebitAccount(balance - amount, startDate, endDate, accountType, accountTariff)
    }
  }

  private def updateTo(to: BankAccount, amount: BigDecimal): BankAccount = {
    to match {
      case SavingAccount(balance, startDate, endDate, accountType, accountTariff) => SavingAccount(balance + amount, startDate, endDate, accountType, accountTariff)
      case DebitAccount(balance, startDate, endDate, accountType, accountTariff) => DebitAccount(balance + amount, startDate, endDate, accountType, accountTariff)
      case DepositAccount(balance, startDate, endDate, accountType, accountTariff) => DepositAccount(balance + amount, startDate, endDate, accountType, accountTariff)
      case CreditAccount(balance, startDate, endDate, accountType, accountTariff) => CreditAccount(balance + amount, startDate, endDate, accountType, accountTariff)
    }
  }

  private def checkSourceBalance(from: BankAccount, amount: BigDecimal): Either[String, Unit] = {
    if (from.balance - amount < 0) Left(s"Not enough balance to charge $amount") else Right()
  }

  private def checkDestinationAccountBalance(to: BankAccount, amount: BigDecimal): Either[String, Unit] = {
    if (to.accountType == Credit && to.balance + amount > 0) Left(s"Recharged amount for credit account cannot exceed it's current balance") else Right()
  }
}
